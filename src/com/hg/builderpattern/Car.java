package com.hg.builderpattern;

public class Car {
    private final String brand;
    private final String model;
    private final int year;
    private boolean sunroof = false;
    private final boolean navigation;
    private final boolean laneSupportSystem;
    private final boolean ledHeadlights;

    public Car(Builder builder) {
        this.brand = builder.brand;
        this.model = builder.model;
        this.year = builder.year;
        this.sunroof = builder.sunroof;
        this.navigation = builder.navigation;
        this.laneSupportSystem = builder.laneSupportSystem;
        this.ledHeadlights = builder.ledHeadlights;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public int getYear() {
        return year;
    }

    public boolean isSunroof() {
        return sunroof;
    }

    public boolean isNavigation() {
        return navigation;
    }

    public boolean isLaneSupportSystem() {
        return laneSupportSystem;
    }

    public boolean isLedHeadlights() {
        return ledHeadlights;
    }

    public static class Builder implements BuilderInterface {
        private final String brand;
        private final String model;
        private final int year;
        private boolean sunroof = false;
        private boolean navigation = false;
        private boolean laneSupportSystem = false;
        private boolean ledHeadlights = false;

        public Builder(String brand, String model, int year) {
            this.brand = brand;
            this.model = model;
            this.year = year;
        }

        @Override
        public Builder hasSunroof(boolean sunroof) {
            this.sunroof = sunroof;
            return this;
        }

        @Override
        public Builder hasNavigation(boolean navigation) {
            this.navigation = navigation;
            return this;
        }

        @Override
        public Builder hasLaneSupportSystem(boolean laneSupportSystem) {
            this.laneSupportSystem = laneSupportSystem;
            return this;
        }

        @Override
        public Builder hasLedHeadlights(boolean ledHeadlights) {
            this.ledHeadlights = ledHeadlights;
            return this;
        }

        @Override
        public Car build() {
            return new Car(this);
        }
    }
}
