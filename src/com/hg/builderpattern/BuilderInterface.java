package com.hg.builderpattern;

public interface BuilderInterface {
    public Car.Builder hasSunroof(boolean sunroof);

    public Car.Builder hasNavigation(boolean navigation);

    public Car.Builder hasLaneSupportSystem(boolean laneSupportSystem);

    public Car.Builder hasLedHeadlights(boolean ledHeadlights);

    public Car build();
}
