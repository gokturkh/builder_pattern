package com.hg.builderpattern;

public class Main {

    public static void main(String[] args) {
        Car car = new Car.Builder("Audi", "A8", 2018)
                .hasSunroof(true)
                .hasNavigation(false)
                .hasLedHeadlights(true)
                .hasLaneSupportSystem(false)
                .build();
        System.out.println(car.getBrand() + " - " + car.getModel() + " - " + car.getYear());
        System.out.println(car.isSunroof() + " - " + car.isNavigation() + " - " + car.isLedHeadlights() + " - " + car.isLaneSupportSystem());
    }
}
